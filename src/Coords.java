package cyk;

/**
 * 
 * @author Matīss Zērvēns
 * @since 22.10.2014
 */
public class Coords implements Cloneable
{
    public int x;
    public int y;
    
    public Coords(int x, int y)
    {
        this.x = x;
        this.y = y;
    }
    
    public Coords()
    {
        this.x = 0;
        this.y = 0;
    }
    
    public void setCoords(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

   @Override
   protected Coords clone() throws CloneNotSupportedException 
   {
      Coords cloned = (Coords)super.clone();
      cloned.setCoords(cloned.x, cloned.y);
      // the above is applicable in case of primitive member types, 
      // however, in case of non primitive types
      // cloned.setNonPrimitiveType(cloned.getNonPrimitiveType().clone());
      return cloned;
   }
    
    
    public void resetX()
    {
        this.x = 0;
    }
    
    public void resetY()
    {
        this.y = 0;
    }
    
    public void decrX()
    {
        if(this.x > 0)
        {
            this.x--;
        }
    }
    
    public void decrY()
    {
        if(this.y > 0)
        {
            this.y--;
        }
    }
    
    public void incrX()
    {
        this.x++;
    }
    
    public void incrY()
    {
        this.y++;
    }

}
