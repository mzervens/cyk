package cyk;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Galvenā programmas klase.
 * Programma ļauj noteikt vai vārds pieder gramatikai vai nē.
 * 
 * Klasē realizēts Cocke–Younger–Kasami(CYK) algoritms, kas
 * ļauj parsēt bezkonteksta gramatikas, kas ir novestas uz CNF.
 * 
 * Teorētiskā algoritma ātrdarbība ir O(n^3), 
 * ceru, ka mana realizācija nav pārāk tālu no tās...
 * 
 * @author Matīss Zērvēns
 * @since 22.10.2014
 */
public class CYK 
{
    private Map<String, Set<String>> grammarProds;
    
    private List<List<String>> wordSubstrProds;
    
    private String emptyNonTerminals;
    
    private String startingSymbol;
    
    public boolean check(String word) throws Exception
    {
        wordSubstrProds = new ArrayList<>(word.length());

        
        Set<String> currentNonTerm;
        String currentVal;
        
        for(int i = 0; i < word.length(); i++)
        {
            wordSubstrProds.add(new ArrayList<String>(word.length()));
            
            if(grammarProds.containsKey(String.valueOf(word.charAt(i))))
            {
                currentNonTerm = grammarProds.get(String.valueOf(word.charAt(i)));
                for(String nonTerm : currentNonTerm)
                {
                    if(wordSubstrProds.get(0).size() < i)
                    {
                        currentVal = wordSubstrProds.get(0).get(i) + ",";
                    }
                    else
                    {
                        currentVal = "";
                    }
                    wordSubstrProds.get(0).add(i, currentVal + nonTerm);
                }
            
            }
            else
            {
                wordSubstrProds.get(0).add(i, emptyNonTerminals);
            }

        }
        
        
        Set <String> compFieldNonTerms = new HashSet<>();
        Set <String> currFieldNonTerms;
        List<Coords[]> fieldSubs;
        
        String fieldNonTerms = "";
        
        for(int i = 1; i < wordSubstrProds.size(); i++)
        {
            for(int n = 0; n < wordSubstrProds.size() - i; n++)
            {
                fieldSubs = stringSubs(new Coords(i, n));
                for(int s = 0; s < fieldSubs.size(); s++)
                {
                    currFieldNonTerms = generatePermutations(fieldSubs.get(s));
                    compFieldNonTerms.addAll(currFieldNonTerms);
                }
                
                for(String v : compFieldNonTerms)
                {
                    fieldNonTerms += ",";
                    fieldNonTerms += v;
                }
                
                if(!(fieldNonTerms.equals("")))
                {
                    fieldNonTerms = fieldNonTerms.substring(1);
                }
                else
                {
                    fieldNonTerms = emptyNonTerminals;
                }
                
                wordSubstrProds.get(i).add(n, fieldNonTerms);
                fieldNonTerms = "";
                compFieldNonTerms = new HashSet<>();
            }

        }
        
        if(wordSubstrProds.get(word.length() - 1).get(0).contains(startingSymbol))
        {
            return true;
        }
        else
        {
            return false;
        }

    }
    
    /**
     * 
     * 
     * @param pair
     * @return 
     */
    private Set<String> generatePermutations(Coords [] pair)
    {
        Set<String> perms = new HashSet<>();
        Set<String> prods;
        
        String field1 = wordSubstrProds.get(pair[0].x).get(pair[0].y);
        String field2 = wordSubstrProds.get(pair[1].x).get(pair[1].y);
        
        if((field1.equals(emptyNonTerminals) || field1.equals("")) || (field2.equals(emptyNonTerminals) || field2.equals("")))
        {
            return perms;
        }
        
        String [] field1Parts = field1.split(",");
        String [] field2Parts = field2.split(",");
        
        
        String perm;
        
        for(int i = 0; i < field1Parts.length; i++)
        {
            for(int n = 0; n < field2Parts.length; n++)
            {
                perm = field1Parts[i] + field2Parts[n];
                if(grammarProds.containsKey(perm))
                {
                    prods = grammarProds.get(perm);
                    for(String prod : prods)
                    {
                        perms.add(prod);
                    }
                }
            }
        }
        
        return perms;
    }
    
    /**
     * Funkcija atgriež koordinātu pāru sarakstu konkrētai tabulas šūnas koordinātei 2d sarakstā wordSubstrProds
     * Katrs pāris sarakstā apzīmē vienu iespējamo variantu kā attiecīgā vārda daļa var tikt uzbūvēta
     * no iepriekš jau apstrādātām vārda daļām.
     * 
     * Piemēram, ja mēs apskatam grammatiku G, un vārdu aabbbb, ja
     * mūs interesē kā var būt vārda daļa [aabb]bb uzģenerēta, tad mums šai funkcijai jāpadod koordinātas x=3 (vārds garumā 4, sākam no nulles) 
     * y=0 (apakšvārda sākuma pozīcija vārdā)
     * 
     * Tiek atgriezti visi iespējamie koordinātu pāri, kas veido mūsu vārda apakšvārdu, šajā gadījumā tie būtu:
     * {x1=0 y1=0, x2=2 y2=1}, {x1=1 y1=0, x2=1 y2=2}, {x1=2 y1=0, x2=0 y2=3}
     * 
     * 
     * 
     * @param source
     * @return
     * @throws Exception 
     */
    private List<Coords[]> stringSubs(Coords source) throws Exception
    {
        List<Coords[]> subs = new ArrayList<>();
        
        if(source.x == 0)
        {
            return subs;
        }
        

        Coords horStart = source.clone();
        Coords diagStart = source.clone();
        
        int index = 0;
        Coords pair [] = new Coords[2];
        
        horStart.resetX();
        diagStart.decrX();
        diagStart.incrY();

        while(horStart.x != source.x)
        {
            pair[0] = new Coords(horStart.x, horStart.y);
            pair[1] = new Coords(diagStart.x, diagStart.y);
            
            subs.add(index, pair);
            pair = new Coords[2];
            index++;
            
            horStart.incrX();
            diagStart.decrX();
            diagStart.incrY();
        
        }
        
        return subs;
    
    }
    
    /**
     * Funkcija izdrukā ģenerēto trīstūrveida tabulu,
     * kas iegūta pārbaudot vārda piederību gramatikai.
     */
    public void printTable()
    {
        for(int i = 0; i < wordSubstrProds.size(); i++)
        {
            for(int n = 0; n < wordSubstrProds.get(i).size(); n++)
            {
                System.out.print(wordSubstrProds.get(i).get(n) + " ");
            }
            System.out.println("");
        }
    }

    public CYK(Map<String, Set<String>> prods, String startSymb)
    {
        grammarProds = prods;
        startingSymbol = startSymb;
        emptyNonTerminals = "-";
    }
    
}
